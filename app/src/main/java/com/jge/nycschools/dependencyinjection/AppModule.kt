package com.jge.nycschools.dependencyinjection

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.jge.nycschools.NYCSchoolsApplication
import com.jge.nycschools.NetworkUtils
import com.jge.nycschools.models.School
import com.jge.nycschools.network.SchoolsApi
import com.jge.nycschools.repository.SchoolRepository
import com.jge.nycschools.repository.SchoolRepositoryImpl
import com.jge.nycschools.viewmodel.ViewModelSchool
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    /**Here we create some singletons so it can be easier to make requests as
     * needed in the while reducing boilerplater code from dagger's dependency injection*/

    @Provides
    @Singleton
    fun provideSchoolsApi():SchoolsApi{
        return Retrofit.Builder()
            .baseUrl(NetworkUtils.SAT_BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build().create(SchoolsApi::class.java)
    }

    @Provides
    @Singleton
    fun providesSchoolRepository(schoolsApi: SchoolsApi, app:NYCSchoolsApplication, listOfSchools:MutableLiveData<List<School>>):SchoolRepository{
        return SchoolRepositoryImpl(schoolsApi, app, listOfSchools)
    }

    @Provides
    @Singleton
    fun provideApplication():NYCSchoolsApplication{
        return NYCSchoolsApplication()
    }

    @Provides
    @Singleton
    fun provideViewModel(schoolRepository: SchoolRepository, app: NYCSchoolsApplication):ViewModelSchool{
        return ViewModelSchool(schoolRepository, app)
    }

    @Provides
    fun provideListOfSchools(): MutableLiveData<List<School>> {
        return MutableLiveData<List<School>>()
    }

    @Provides
    fun provideSATScores(): MutableLiveData<School>{
        return MutableLiveData<School>()
    }
}