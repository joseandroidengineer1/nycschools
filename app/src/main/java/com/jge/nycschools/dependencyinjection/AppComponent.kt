package com.jge.nycschools.dependencyinjection

import com.jge.nycschools.activities.DetailsActivity
import com.jge.nycschools.activities.MainActivity
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class])
interface AppComponent {
    fun inject(mainActivity: MainActivity?)
    fun inject(detailsActivity: DetailsActivity?)
}