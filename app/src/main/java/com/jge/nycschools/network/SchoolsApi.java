package com.jge.nycschools.network;

import com.jge.nycschools.models.SAT;
import com.jge.nycschools.models.School;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface SchoolsApi {
    @GET("resource/s3k6-pzi2.json")
    Call<List<School>> getListOfSchools();

    @GET("resource/s3k6-pzi2.json")
    Call<School> getSchoolDetails(@Query("dbn") String dbn);

    @GET("resource/734v-jeq5.json")
    Call<List<SAT>> getSATScores(@Query("dbn") String dbn);
}
