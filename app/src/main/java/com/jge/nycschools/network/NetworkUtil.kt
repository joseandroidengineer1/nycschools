package com.jge.nycschools

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.Uri
import android.net.wifi.WifiManager
import android.os.Build
import android.provider.Settings
import androidx.core.app.ActivityCompat.startActivityForResult
import com.jge.nycschools.network.SchoolsApi
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class NetworkUtils {
    companion object {
        const val SAT_BASE_URL = "https://data.cityofnewyork.us/"
        const val WIFI_SETTING = 143
        private val retrofit = Retrofit.Builder()
                .baseUrl(SAT_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
         fun getInterface(): SchoolsApi {
            return retrofit.create(SchoolsApi::class.java)
        }

        /**If there was more time allowed I would've used Android's most recent best practice in
         * checking for internet connection. I know some developers say to ping something like wwww.Google.com,
         * but I don't think that sounds right with me. Please pardon me with the deprecation.**/
        fun isInternetAvailable(context: Context): Boolean {
            val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val activeNetwork = cm.activeNetworkInfo
            return activeNetwork != null && activeNetwork.isConnectedOrConnecting
        }

        /**If more time was allowed I would've also implemented a method to prompt the user if they
         * also want to turn on their cell service, or if user is already on cell service and they are
         * about to make a heavy network request we can show a dialog to prompt if they want to continue
         * or if they want to connect to their WiFi instead.**/
        fun turnOnWifi(activity: Activity){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                val panelIntent = Intent(Settings.Panel.ACTION_WIFI)
                activity.startActivityForResult(panelIntent, WIFI_SETTING)
            } else {
                val wmgr = activity.application.applicationContext.getSystemService(Context.WIFI_SERVICE) as WifiManager
                wmgr.isWifiEnabled = true
            }
        }
    }


}