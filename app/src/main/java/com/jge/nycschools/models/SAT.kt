package com.jge.nycschools.models

import com.google.gson.annotations.SerializedName

class SAT {
    var dbn:String = ""
    @SerializedName("school_name")
    var schoolName:String="N/A"
    @SerializedName("num_of_sat_test_takers")
    var numOfSatTestTakers:String ="N/A"
    @SerializedName("sat_critical_reading_avg_score")
    var satCriticalReadingAvgScore:String ="N/A"
    @SerializedName("sat_math_avg_score")
    var satMathAvgScore:String="N/A"
    @SerializedName("sat_writing_avg_score")
    var satWritingAvgScore:String="N/A"

}