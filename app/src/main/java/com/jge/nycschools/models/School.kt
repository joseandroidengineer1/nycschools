package com.jge.nycschools.models

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

class School() : Parcelable{
    private var dbn: String? = null

    @SerializedName("school_name")
    private var schoolName: String? = null

    @SerializedName("overview_paragraph")
    private var overview: String? = null

    private var borough: String? = null

    @SerializedName("school_email")
    private var schoolEmail: String? = null

    @SerializedName("phone_number")
    private var phoneNumber: String? = null

    constructor(parcel: Parcel) : this() {
        dbn = parcel.readString()
        schoolName = parcel.readString()
        overview = parcel.readString()
        borough = parcel.readString()
        schoolEmail = parcel.readString()
        phoneNumber = parcel.readString()
    }

    fun getSchoolName(): String? {
        return schoolName
    }

    fun setSchoolName(schoolName: String?) {
        this.schoolName = schoolName
    }

    fun getOverview(): String? {
        return overview
    }

    fun setOverview(overview: String?) {
        this.overview = overview
    }

    fun getBorough(): String? {
        return borough
    }

    fun setBorough(borough: String?) {
        this.borough = borough
    }

    fun getSchoolEmail(): String? {
        return schoolEmail
    }

    fun setSchoolEmail(schoolEmail: String?) {
        this.schoolEmail = schoolEmail
    }

    fun getPhoneNumber(): String? {
        return phoneNumber
    }

    fun setPhoneNumber(phoneNumber: String?) {
        this.phoneNumber = phoneNumber
    }

    fun getDbn(): String? {
        return dbn
    }

    fun setDbn(dbn: String?) {
        this.dbn = dbn
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(dbn)
        parcel.writeString(schoolName)
        parcel.writeString(overview)
        parcel.writeString(borough)
        parcel.writeString(schoolEmail)
        parcel.writeString(phoneNumber)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<School> {
        override fun createFromParcel(parcel: Parcel): School {
            return School(parcel)
        }

        override fun newArray(size: Int): Array<School?> {
            return arrayOfNulls(size)
        }
    }
}