package com.jge.nycschools

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class NYCSchoolsApplication:Application() {

    override fun onCreate() {
        super.onCreate()
    }

}