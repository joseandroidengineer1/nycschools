package com.jge.nycschools.viewmodel

import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.jge.nycschools.NYCSchoolsApplication
import com.jge.nycschools.models.SAT
import com.jge.nycschools.models.School
import com.jge.nycschools.repository.SchoolRepository
import javax.inject.Inject


class ViewModelSchool @Inject constructor(
    private val repository: SchoolRepository,
    application: NYCSchoolsApplication
): AndroidViewModel(application) {

   fun loadListOfSchoolsRepo(): LiveData<List<School>> {
        return repository.getListOfSchools()
    }


    fun loadSatScores(dbn: String): LiveData<List<SAT>> {
        return repository.getSATScores(dbn)
    }


}