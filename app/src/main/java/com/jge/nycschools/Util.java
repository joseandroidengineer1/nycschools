package com.jge.nycschools;

public class Util {
    public static boolean checkIfObjectIsNonNull(Object object){
        if(object instanceof String){
            checkIfStringIsEmpty((String)object);
        }else return object != null;
        return false;
    }

    private static boolean checkIfStringIsEmpty(String object) {
        return !object.equals("");
    }
}
