package com.jge.nycschools

import com.jge.nycschools.models.School

interface OnSchoolListListener {

    fun onSchoolClick(school: School)
}