package com.jge.nycschools.activities;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.jge.nycschools.NetworkUtils;
import com.jge.nycschools.OnSchoolListListener;
import com.jge.nycschools.R;
import com.jge.nycschools.Util;
import com.jge.nycschools.adapters.SchoolAdapter;
import com.jge.nycschools.dependencyinjection.AppComponent;
import com.jge.nycschools.dependencyinjection.DaggerAppComponent;
import com.jge.nycschools.models.School;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class MainActivity extends AppCompatActivity implements OnSchoolListListener {

    private List<School> listOfSchools = new ArrayList<>();
    //private ViewModelSchool viewModelSchool;
    private RecyclerView recyclerView;
    private ProgressBar progressBar;
    private SchoolAdapter adapter;
    private TextView errorTextView;
    private LinearLayout errorLayout;
    private Button reconnectButton;
    private AppComponent appComponent;

    @Inject
    com.jge.nycschools.viewmodel.ViewModelSchool viewModelSchool;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        appComponent = DaggerAppComponent.builder().build();
        appComponent.inject(this);
        initViews();
        if(NetworkUtils.Companion.isInternetAvailable(this)){
            startNetworkConnection();
        }else{
            handleUIWhenNoInternetConnection();
        }
        setUpRecyclerView();
    }

    private void handleUIWhenNoInternetConnection() {
        errorLayout.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);
        errorTextView.setText(getResources().getString(R.string.error_msg_school));
    }

    private void startNetworkConnection() {
        viewModelSchool.loadListOfSchoolsRepo();
        viewModelSchool.loadListOfSchoolsRepo().observe(this, response -> {
            if(Util.checkIfObjectIsNonNull(response) && response.size() > 0){
                listOfSchools = response;
                handleUIWhenApiSucceeds();
                adapter.setListOfSchools(listOfSchools);
            }else{
                handleUIWhenApiFails();
            }
        });
    }

    private void handleUIWhenApiSucceeds() {
        errorLayout.setVisibility(View.GONE);
        progressBar.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);
    }

    private void hideErrorUI(){
        errorLayout.setVisibility(View.GONE);
    }

    private void handleUIWhenApiFails() {
        progressBar.setVisibility(View.GONE);
        errorLayout.setVisibility(View.VISIBLE);
        errorTextView.setVisibility(View.VISIBLE);
        reconnectButton.setVisibility(View.GONE);
    }

    private void initViews() {
       recyclerView = findViewById(R.id.recycler_view);
       progressBar = findViewById(R.id.pb_loading_indicator);
       errorTextView = findViewById(R.id.tv_error_message_display);
       errorLayout = findViewById(R.id.error_layout);
       reconnectButton = findViewById(R.id.reconnect_btn);
       initListeners();
    }

    private void initListeners() {
        reconnectButton.setOnClickListener(v -> NetworkUtils.Companion.turnOnWifi(this));
    }

    private void setUpRecyclerView() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);

        adapter = new SchoolAdapter(listOfSchools,this);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onSchoolClick(@NotNull School school) {
        Intent intent = new Intent(this, DetailsActivity.class);
        intent.putExtra(DetailsActivity.SCHOOL_OBJ, school);
        startActivity(intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        /**In the case user is not connected to the internet and they reconnect and come back to
         * this screen the app will automatically to try to fetch the data it failed to fetch because
         * of network issues.**/
        if(requestCode == NetworkUtils.WIFI_SETTING){
            if(Util.checkIfObjectIsNonNull(progressBar)){
                progressBar.setVisibility(View.VISIBLE);
            }
            hideErrorUI();
            startNetworkConnection();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.search_menu, menu);
        MenuItem searchItem = menu.findItem(R.id.actionSearch);
        SearchView searchView = (SearchView) searchItem.getActionView();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                filter(newText);
                return false;
            }
        });
        return true;
    }

    private void filter(String text) {
        ArrayList<School> filteredList = new ArrayList<>();

        for (School item : listOfSchools) {
            if (item.getSchoolName().toLowerCase().contains(text.toLowerCase())) {
                filteredList.add(item);
            }
        }
        if (filteredList.isEmpty()) {
            Toast.makeText(this, "School not available...", Toast.LENGTH_SHORT).show();
        } else {
            adapter.setListOfSchools(filteredList);
        }
    }
}