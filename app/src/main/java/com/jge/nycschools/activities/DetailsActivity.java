package com.jge.nycschools.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.jge.nycschools.R;
import com.jge.nycschools.Util;
import com.jge.nycschools.dependencyinjection.AppComponent;
import com.jge.nycschools.dependencyinjection.DaggerAppComponent;
import com.jge.nycschools.models.SAT;
import com.jge.nycschools.models.School;
import com.jge.nycschools.viewmodel.ViewModelSchool;

import java.util.List;

import javax.inject.Inject;

/**This Activity is meant to show more details about the school selected
 * and information about SAT Testing
 *
 * If provided with more time I would've used the Room to save the schools selected by having users
 * favorite them so they can take another look at it later without having to access the internet**/
public class DetailsActivity extends AppCompatActivity {
    public static final String SCHOOL_OBJ = "school_obj";
    private TextView numOfSATTestTakersTV, avgReadingScoreTV, avgMathScoreTV, avgWritingScoreTV,satLabel;
    private TextView schoolNameTV, overviewTV, locationTV;
    private String testTakers = "This information is not available right now";
    private String avgReading = "This information is not available right now";
    private String avgMath = "This information is not available right now";
    private String avgWriting = "This information is not available right now";
    private School school;
    private AppComponent appComponent;

    @Inject
    ViewModelSchool viewModelSchool;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        appComponent = DaggerAppComponent.builder().build();
        appComponent.inject(this);
        getIntentData();
        initViews();
        viewModelSchool.loadSatScores(school.getDbn())
                .observe(this, response->{
            populateStrings(response);
            populateUI();
        });
    }

    private void populateUI() {
        numOfSATTestTakersTV.setText(testTakers);
        avgReadingScoreTV.setText(avgReading);
        avgMathScoreTV.setText(avgMath);
        avgWritingScoreTV.setText(avgWriting);
        schoolNameTV.setText(school.getSchoolName());
        overviewTV.setText(school.getOverview());
        locationTV.setText(school.getBorough());
    }

    private void populateStrings(List<SAT> response) {
        if(Util.checkIfObjectIsNonNull(response) && response.size() == 1){
            SAT satScores = response.get(0);
            testTakers = "Number of Test Takers: " + satScores.getNumOfSatTestTakers();
            avgReading = "Average Reading Score: " + satScores.getSatCriticalReadingAvgScore();
            avgMath    = "Average Math Score: "    + satScores.getSatMathAvgScore();
            avgWriting = "Average Writing Score: " + satScores.getSatWritingAvgScore();
            showSATNumbers();
        }else{
            hideSATNumbers();
        }
    }

    private void hideSATNumbers() {
        numOfSATTestTakersTV.setVisibility(View.GONE);
        avgWritingScoreTV.setVisibility(View.GONE);
        avgMathScoreTV.setVisibility(View.GONE);
        avgReadingScoreTV.setVisibility(View.GONE);
        satLabel.setVisibility(View.GONE);
    }

    private void showSATNumbers(){
        numOfSATTestTakersTV.setVisibility(View.VISIBLE);
        avgWritingScoreTV.setVisibility(View.VISIBLE);
        avgMathScoreTV.setVisibility(View.VISIBLE);
        avgReadingScoreTV.setVisibility(View.VISIBLE);
        satLabel.setVisibility(View.VISIBLE);
    }

    /**We use this to get the Parcelable Object of School so we can pass whatever information we want to this screen
     * If the parcelable object proved to be too big however, we would need to do a network request to get more information
     * and details about the school.
     */
    private void getIntentData() {
        school = getIntent().getParcelableExtra(SCHOOL_OBJ);
    }

    private void initViews() {
        numOfSATTestTakersTV = findViewById(R.id.num_of_sat_takers);
        avgReadingScoreTV = findViewById(R.id.avg_reading_score);
        avgMathScoreTV = findViewById(R.id.avg_of_math_score);
        avgWritingScoreTV = findViewById(R.id.avg_of_writing_score);
        schoolNameTV = findViewById(R.id.detail_school_name);
        overviewTV = findViewById(R.id.overview);
        locationTV = findViewById(R.id.detail_borough);
        satLabel = findViewById(R.id.sat_label_tv);
    }
}
