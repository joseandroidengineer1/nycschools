package com.jge.nycschools.adapters;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.jge.nycschools.R;

public class SchoolViewHolder extends RecyclerView.ViewHolder {
    public TextView schoolNameTextView;
    public TextView schoolBoroughTextView;
    public TextView schoolPhoneTextView;
    public SchoolViewHolder(@NonNull View itemView) {
        super(itemView);
        schoolNameTextView = itemView.findViewById(R.id.school_name);
        schoolBoroughTextView = itemView.findViewById(R.id.borough);
        schoolPhoneTextView = itemView.findViewById(R.id.phone_tv);
    }
}
