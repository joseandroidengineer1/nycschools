package com.jge.nycschools.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.jge.nycschools.OnSchoolListListener;
import com.jge.nycschools.R;
import com.jge.nycschools.models.School;
import com.jge.nycschools.Util;

import java.util.List;

public class SchoolAdapter extends RecyclerView.Adapter<SchoolViewHolder>{
    private List<School> listOfSchools;
    private final OnSchoolListListener onSchoolListListener;
    /**Here we use dependency injection for the adapter because we'll need the list of schools
     * to map on the recyclerview and pass back the school that was clicked so that the onClick callback
     * in the MainActivity take that data and handle what it needs to handle**/
    public SchoolAdapter(List<School> schools, OnSchoolListListener onSchoolListListener){
        listOfSchools = schools;
        this.onSchoolListListener = onSchoolListListener;
    }

    @NonNull
    @Override
    public SchoolViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        int layoutIdForListItem = R.layout.school_list_item;
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(layoutIdForListItem, parent, false);
        return new SchoolViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SchoolViewHolder holder, int position) {
        School school = listOfSchools.get(position);
        holder.schoolBoroughTextView.setText(school.getBorough());
        holder.schoolNameTextView.setText(school.getSchoolName());
        holder.schoolPhoneTextView.setText(school.getPhoneNumber());
        holder.itemView.setOnClickListener(v -> onSchoolListListener.onSchoolClick(school));
    }

    @Override
    public int getItemCount() {
        if(Util.checkIfObjectIsNonNull(listOfSchools) && listOfSchools.size() > 0){
            return listOfSchools.size();
        }
        return 0;
    }

    public void setListOfSchools(List<School> listOfSchools){
        this.listOfSchools = listOfSchools;
        notifyDataSetChanged();
    }
}
