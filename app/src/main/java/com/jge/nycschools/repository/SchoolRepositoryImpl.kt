package com.jge.nycschools.repository

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.jge.nycschools.R
import com.jge.nycschools.models.SAT
import com.jge.nycschools.models.School
import com.jge.nycschools.network.SchoolsApi
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

class SchoolRepositoryImpl @Inject constructor(
    private val schoolsApi: SchoolsApi,
    private val appContext: Application,
    private val listOfSchools:MutableLiveData<List<School>>
) : SchoolRepository {

    private val schoolDetails = MutableLiveData<School>()
    private val satScores = MutableLiveData<List<SAT>>()

    override fun getListOfSchools(): MutableLiveData<List<School>> {
        val listOfSchoolsOut = schoolsApi.listOfSchools
        listOfSchoolsOut.enqueue(object : Callback<List<School>> {
            override fun onResponse(
                call: Call<List<School>>,
                response: Response<List<School>>
            ) {
                listOfSchools.value = response.body()
            }

            override fun onFailure(call: Call<List<School>>, t: Throwable) {
                listOfSchools.postValue(null)
            }
        })
        return listOfSchools
    }

    override fun getSchoolDetails() {
        TODO("Not yet implemented")
    }

    override fun getSATScores(dbn:String): MutableLiveData<List<SAT>> {
        val satDetailsOutput = schoolsApi.getSATScores(dbn)
        satDetailsOutput.enqueue(object : Callback<List<SAT>> {
            override fun onResponse(call: Call<List<SAT>>, response: Response<List<SAT>>) {
                satScores.value = response.body()
            }

            override fun onFailure(call: Call<List<SAT>>, t: Throwable) {
                satScores.postValue(null)
            }
        })
        return satScores
    }

}