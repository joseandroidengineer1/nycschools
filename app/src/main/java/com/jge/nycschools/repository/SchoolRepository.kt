package com.jge.nycschools.repository

import androidx.lifecycle.MutableLiveData
import com.jge.nycschools.models.SAT
import com.jge.nycschools.models.School

interface SchoolRepository {
    fun getListOfSchools(): MutableLiveData<List<School>>
    fun getSchoolDetails()
    fun getSATScores(dbn:String): MutableLiveData<List<SAT>>
}